# Projet Cloud IoT
### Groupe : Maxime SIX, Matthew JOY

# Phase 1

## Cahier des Charges (CDC)

### I. Introduction

L'Université Paris-Est Créteil (UPEC), une institution renommée du domaine de l'Éducation et de la Recherche, s'est positionnée comme un acteur incontournable dans le paysage éducatif français. Avec une gamme diversifiée de services, allant des cours en ligne aux recherches académiques, l'UPEC joue un rôle central dans la formation de la prochaine génération de professionnels et chercheurs.
Sa structure informatique traditionnelle comprend :


**Structure informatique traditionnelle :**
- Serveurs locaux : Ces serveurs sont essentiellement dédiés à la gestion des étudiants. Ils centralisent les informations relatives à l'inscription, aux notes, et à la progression académique.
- Bases de données académiques : Elles conservent une vaste quantité de données de recherche, de publications et d'archives historiques, formant un trésor inestimable pour l'université.
- Réseau LAN/Wi-Fi : Essentiel pour une institution de cette taille, il garantit une connectivité continue pour le corps enseignant, le personnel administratif et les étudiants.
- Systèmes de sauvegarde centralisés : Pour éviter toute perte de données, ces systèmes sont en place pour garantir la sécurité et l'intégrité des informations essentielles.

### II. Analyse des Besoins et Contraintes

#### Besoins spécifiques de l'UPEC :
- Haute disponibilité : Les plateformes et les services utilisés par les étudiants, tels que les portails d'inscription ou d'e-learning, ne doivent connaître aucune interruption.
- Scalabilité : Chaque année, de nouveaux étudiants s'inscrivent, de nouvelles recherches sont entreprises et de nouveaux cours sont proposés. L'infrastructure cloud doit être capable de s'adapter rapidement à ces changements.
- Performance : La recherche académique, en particulier dans des domaines avancés comme la simulation ou la bio-informatique, nécessite des temps de traitement rapides.

#### Discussion des contraintes :
- Sécurité : Les informations sur les étudiants et certaines recherches sensibles sont de nature délicate. Une divulgation ou un accès non autorisé pourrait avoir des répercussions graves, tant sur le plan légal qu'éthique.
- Disponibilité : Disponibilité constante pour des services tels que l'inscription. Période critique avec beaucoup de connexions.
- Performance : Rapidité et réactivité pour l'accès aux ressources.

### III. Choix de la Solution

**Nécessité d'une Solution Hybride :**
- Cloud Privé : Stockage des données confidentielles et systèmes essentiels.
- Cloud Public : Utilisation d'outils comme Microsoft Teams et autres applications non critiques.

**Argumentation pour une Solution Hybride :**
- Combiner le meilleur des clouds privé et public pour offrir flexibilité, scalabilité, sécurité et innovations.

**Considérations Techniques**
- Mise en place d'une connectivité robuste, d'outils de gestion unifiés et de politiques de sécurité cohérentes.

### IV. Solutions de Cloud Computing

#### 1. Modèles de Services

##### IaaS (Infrastructure en tant que Service)
- **Description** : Il offre une infrastructure informatique virtualisée via Internet. Avec IaaS, vous pouvez louer des ressources informatiques, telles que des serveurs ou des espaces de stockage, sur une base d'abonnement.

- **Exemples:**
  - OpenStack : C'est une plateforme open source qui permet de déployer des services d'infrastructure de cloud. Elle offre une flexibilité sans équivalent, notamment pour les institutions qui souhaitent une totale maîtrise de leur infrastructure.
  - Azure Stack : Microsoft propose cette solution pour ceux qui souhaitent exploiter Azure dans leur propre datacenter. C'est particulièrement utile pour les entités qui ont besoin de conserver des données sensibles en interne tout en bénéficiant des avantages de la plateforme Azure.

##### PaaS (Platform as a Service)
- **Description** : Il offre un environnement pour le développement, le test, la livraison et la gestion d'applications. PaaS est particulièrement utile pour le développement d'applications sans la complexité de la gestion de l'infrastructure sous-jacente.
 
- **Exemples:**
  - OpenShift : Plateforme PaaS open-source basée sur Kubernetes, offerte par Red Hat. Elle est idéale pour développer et déployer rapidement des applications.
  - Azure App Service : Service de Microsoft Azure offrant des capacités PaaS pour le développement et l'hébergement d'applications web.

#### 2. Modèle de Déploiement

##### Public
- **Description** : Les ressources sont hébergées sur les serveurs du fournisseur de cloud et sont partagées avec d'autres clients.

- **Utilisation** : Potentiel pour des applications non sensibles, telles que les sites web institutionnels ou les plateformes de diffusion de contenus publics.

##### Privé
- **Description** :  Les ressources cloud sont utilisées exclusivement par une seule organisation. C'est un environnement hautement sécurisé et contrôlé.
- **Exemples:** 
  - OpenStack : Très apprécié pour sa flexibilité, il est souvent utilisé pour déployer des clouds privés.
  - Azure Stack : Cette solution de Microsoft permet de construire un cloud privé avec les mêmes outils que le cloud public Azure, tout en garantissant un contrôle total sur les données.

##### Hybride
- **Description** :  Combine des éléments des clouds publics et privés, permettant à une organisation de bénéficier du meilleur des deux mondes.

- **Utilisation** : Pour une institution comme l'UPEC, cela pourrait permettre d'héberger des données sensibles en interne tout en exploitant la puissance et la flexibilité des solutions cloud publiques pour certaines applications.

#### 3. Solutions Propriétaires et Open Source
- OpenStack : Il s'agit d'une solution open-source qui a évolué pour devenir une référence dans le déploiement de clouds privés. Ses nombreux modules permettent une adaptation fine aux besoins spécifiques des utilisateurs.
- MS Azure (et Azure Stack) : Azure est la solution cloud de Microsoft et est largement adoptée par de nombreuses organisations. Azure Stack, comme mentionné précédemment, permet de bénéficier des avantages d'Azure dans un environnement privé.
- AWS et AWS Outposts : AWS, le leader du marché cloud, a introduit AWS Outposts pour répondre à la demande d'infrastructures de cloud privé. Outposts étend les outils et services AWS à l'infrastructure on-premises, offrant ainsi une véritable expérience hybride.

### V. Recommandation

#### 1. Évaluation Initiale

Face à l'évolution rapide des besoins informatiques, il est essentiel pour l'UPEC d'opter pour une solution adaptée à son contexte. Une analyse approfondie a montré que l'UPEC a des besoins mixtes en matière de cloud, allant du stockage des données sensibles des étudiants à l'utilisation d'outils de collaboration en temps réel comme Microsoft Teams.

#### 2. Nécessité d'une Solution Hybride

##### Cloud Privé
- **Avantages** : Une meilleure sécurité, un contrôle total des ressources, et une personnalisation en fonction des besoins. 
- **Usage à l'UPEC** : Idéal pour le stockage des données des étudiants, la recherche académique, et les systèmes essentiels tels que les inscriptions et les bases de données.

##### Cloud Public
- **Avantages** : Coûts réduits, scalabilité, accès à des services et outils innovants.
- **Usage à l'UPEC** : Des outils comme Microsoft Teams, qui sont essentiels pour la collaboration et l'e-learning, sont hébergés en cloud public. De plus, certaines applications non critiques peuvent être déployées sur le cloud public pour profiter de sa flexibilité.

#### 3. Argumentation pour une Solution Hybride

Le principal avantage de l'adoption d'une solution hybride est qu'elle combine le meilleur des deux mondes. L'UPEC peut maintenir la confidentialité et le contrôle de ses données les plus sensibles tout en bénéficiant de la flexibilité, de la scalabilité et des innovations du cloud public. De plus, cela permet de:
- Éviter les coûts d'investissement initiaux lourds en hardware.
- Adapter la capacité en fonction des besoins.
- Profiter d'une continuité d'activité optimale en répartissant les charges entre le cloud privé et public.

#### 4. Considérations Techniques

Pour une transition en douceur vers une solution hybride, il est crucial d'avoir :
- Une bonne connectivité réseau entre le cloud privé et public.
- Des outils de gestion unifiés pour surveiller et administrer les ressources dans les deux environnements.
- Des politiques de sécurité cohérentes pour s'assurer que les données se déplacent de manière sécurisée entre les deux environnements.

#### 5. Conclusion sur le Choix

Compte tenu de la complexité et de la variété des besoins de l'UPEC, une solution de cloud hybride s'impose comme le choix le plus logique et le plus avantageux. Elle permettra à l'université de rester agile tout en garantissant la sécurité et l'intégrité de ses ressources critiques.

### VI. Conclusion

Le cloud hybride offre à l'UPEC la meilleure combinaison de sécurité, flexibilité et innovation pour répondre aux besoins actuels et futurs de l'éducation et de la recherche.

